## Minimal Working Example for OpenMP variants

It displays a hello world depending on where the task has been executed.

It has the following dependencies:

### StarPU

Branch `llvm-openmp`.

Configure: `../configure --prefix=/home/fifi/install/starpu-variants CC=gcc CXX=g++ F77=gfortran FC=gfortran --enable-openmp --disable-mpi`

### LLVM

Any official fork will do (eg: https://git.llvm.org/git/llvm.git/).

Branch `release_39`.

### Clang

To be cloned in the llvm/tools directory.

Version from the forge: `git+ssh://LOGIN@scm.gforge.inria.fr/gitroot/kstar/clang-omp.git`.

Branch `rel-39-variants`.

CMake: `cmake .. -DCMAKE_BUILD_TYPE=release -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_INSTALL_PREFIX=$HOME/install/clang-kstar`.

### LLVM OpenMP Runtime

To have the empty endpoints.

Git: `git@gitlab.inria.fr:viroulea/llvm-openmp.git`.

Branch `variants`.

CMake: `cmake .. -DCMAKE_BUILD_TYPE=release -DCMAKE_INSTALL_PREFIX=$HOME/install/clang-kstar`

(installed to the same place as the clang compiler)


### Compile and run the thing

  - `make`
  - Run by preloading starpu: `LD_PRELOAD=$HOME/install/starpu-variants/lib/libstarpu-1.3.so STARPU_NCPU=1 OMP_NUM_THREADS=1 ./hello_variants`

It should display "hello from gpu kernel".
