#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>

void print_vector(double *v, size_t n, char *name)
{
  printf("Vector %s: [", name);
  for (size_t i = 0; i < n; i++)
    printf("%.2f ", v[i]);
  printf("]\n");
}

void hello_cpu(double *a, size_t *n)
{
  printf("Executing on CPU, multiplying the stride %p:%zu by 3\n", a, *n);
  for (int i = 0; i < *n; i++) {
    a[i] *= 3;
  }
}

#pragma omp declare variant(hello_cpu) match(cuda)
extern void hello_cuda(double *, size_t *);

int main()
{
  size_t n = 32;
  size_t stride = n/2;
  double *a = malloc(n * sizeof(double));
  srand(time(NULL));
  for (size_t i = 0; i < n; i++)
    a[i] = rand() % 100;
  printf("Entry points for functions:\n");
  printf("hello_cpu: %p\n", hello_cpu);
  printf("hello_cuda: %p\n", hello_cuda);
  print_vector(a, n, "a");

#pragma omp parallel
#pragma omp master
  {
    // NOTE: we don't give the same address
    // NOTE: due to current's implementation limitation,
    // the dependency must be the expression given as function arg.
#pragma omp task depend(inout:a[0:stride], stride)
    hello_cpu(&a[0], &stride);
#pragma omp task depend(inout:a[stride:stride], stride)
    hello_cpu(&a[stride], &stride);
  }

  printf("Vector after transformation:\n");
  print_vector(a, n, "a");

  return 0;
}
