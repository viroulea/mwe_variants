#include <stdio.h>
#include <omp.h>

static __global__ void compute_stuff(double *a, size_t *n)
{
  printf("Executing on GPU, multiplying the stride %p:%i by 2\n", a, *n);
  for (int i = 0; i < *n; i++) {
    a[i] *= 2;
  }
}

extern "C" void hello_cuda(double *a, size_t *n)
{
  compute_stuff<<<1, 1, 0, (cudaStream_t)omp_get_local_cuda_stream()>>>(a, n);
  cudaStreamSynchronize((cudaStream_t)omp_get_local_cuda_stream());
}
