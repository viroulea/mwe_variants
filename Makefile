CXX=clang++
CC=clang
CUDA_PATH ?= "/usr/lib/nvidia-cuda-toolkit"
NVCC          := $(CUDA_PATH)/bin/nvcc -ccbin $(HOST_COMPILER)

all: hello_variants

hello_cuda.so: hello_cuda.cu
	nvcc $(CFLAGS) --shared -o hello_cuda.so hello_cuda.cu --compiler-options '-fPIC'

main.o: main.c
	$(CC) -fopenmp main.c -c -o main.o

hello_variants: main.o hello_cuda.so
	$(CC) -fopenmp $^ -o $@ $(LDFLAGS)

clean:
	rm -rf *.o *.so hello_variants

